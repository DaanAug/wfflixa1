<?php
#session maken voor als de user ingelogd is
session_start();
if(!isset($_SESSION["username"])) {
    header("Location: login");
    exit();
}
?>