<?php
require('models/db.php');
require('views/addCourse.view.php');
// When form submitted, insert values into the database.
if (isset($_POST['submit'])) {


    $title = stripslashes($_REQUEST['title']);
    $description = stripslashes($_REQUEST['description']);

    $sql = "INSERT into courses (title, description)
                     VALUES ('$title', '$description')";


    $result = $con->query($sql);

}



?>

<script>
    // zorgt ervoor dat de form niet resubmit wanneer je de pagina refreshed
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }

    // zorgt ervoor dat je de image kan zien voordat je hem upload
    const image_input = document.querySelector("#image_input");
    var uploaded_image;

    image_input.addEventListener('change', function() {
        const reader = new FileReader();
        reader.addEventListener('load', () => {
            uploaded_image = reader.result;
            document.querySelector("#display_image").style.backgroundImage = `url(${uploaded_image})`;
        });
        reader.readAsDataURL(this.files[0]);
    });
</script>

