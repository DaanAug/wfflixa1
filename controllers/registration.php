
<?php
require('models/db.php');
// When form submitted, insert values into the database.
if (isset($_REQUEST['username']))
{

    $username = stripslashes($_REQUEST['username']);
    $email    = stripslashes($_REQUEST['email']);
    $password = stripslashes($_REQUEST['password']);
    $address  = stripslashes($_REQUEST['address']);
    $zipcode  = stripslashes(str_replace(' ', '', ($_REQUEST['zipcode'])));
    $phone    = stripslashes($_REQUEST['phone']);


    #check if username already exists
    $sqlCheckUsername = "SELECT COUNT(*) FROM users WHERE username='$username'";
    $usrResult = $con->query($sqlCheckUsername);
    $count = $usrResult->fetchColumn();
    //die(var_dump($count));
    if ($count < 1) {
        #insert into users

        $sql = "INSERT into users (username, password, email)
                     VALUES (:username, :password, :email)";
        //Prepare statement so there is no injection possible
        $stmt = $con->prepare($sql);
        //md5 is a method to hash a string.
        $hashedPassword = md5($password);

        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':password', $hashedPassword, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $result = $stmt->execute();


        #select ID from users
        $sqlId = "SELECT * FROM users WHERE username = '$username'";
        $idResult = $con->query($sqlId);
        $idResult = $idResult->fetch();
        $userid = $idResult['userID'];

        #Insert into profile
        $sqlprofile = "INSERT INTO profile (userid, address, zipcode, phone)
                        VALUES (:userid,:address,:zipcode,:phone)";
        $profstmt = $con->prepare($sqlprofile);

        $profstmt->bindParam(':userid', $userid, PDO::PARAM_INT);
        $profstmt->bindParam(':address', $address, PDO::PARAM_STR);
        $profstmt->bindParam(':zipcode', $zipcode, PDO::PARAM_STR);
        $profstmt->bindParam(':phone', $phone, PDO::PARAM_STR);
        $profileResult = $profstmt->execute();

    }
    else{echo '<script>alert("Gebruikersnaam bestaat al");window.location.href = "registration";</script>';}


    #Confirm

    if ($result && $profileResult)
    {
        echo '<script>alert("U heeft succesvol een nieuw account aangemaakt");window.location.href = "login";</script>';
    }
    else
    {
        echo '<script>alert("Registratie mislukt, probeer het opnieuw");window.location.href = "registration";</script>';
    }
}
else
    {
        require('views/registration.view.php');
    }
?>