<?php
require('models/db.php');
if(session_status() === PHP_SESSION_NONE) {
    session_start();
}

//die(var_dump($_SESSION));

if(isset($_POST['orderacc'])){
    header("location:Betalen");

    $userid=$_POST['id'];
    $sql="insert into orders (userid)
    values($userid)";
    $con->exec($sql);

    $sql="SELECT * FROM orders ORDER BY id DESC LIMIT 1";
    $result=$con->query($sql);
    $result=$result->fetch();
    $orderid=$result['id'];
    
    foreach ($_SESSION['cart_item']as $product){
        $id=$product['id'];
        $qty=$product['quantity'];
        $sql="insert into orderedproducts (orderid, productid, quantity)
        values ($orderid,$id, $qty)";
        $con->exec($sql);
    }
}

if(isset($_POST['orderguest'])){
        header("location:Betalen");

//    die(var_dump($_POST));
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $address=$_POST['address'];
    $zipcode=$_POST['zipcode'];
    $sql="insert into guests(firstname, lastname, email, address, zipcode) 
    values ('$firstname', '$lastname', '$email', '$address', '$zipcode')";
    $con->exec($sql);

    $sql="SELECT * FROM guests ORDER BY id DESC LIMIT 1";
    $result=$con->query($sql);
    $result=$result->fetch();
    $guestid=$result['id'];

    $sql="insert into guestorders (guestid)
    values($guestid)";
    $con->exec($sql);

    $sql="SELECT * FROM guestorders ORDER BY id DESC LIMIT 1";
    $result=$con->query($sql);
    $result=$result->fetch();
    $orderid=$result['id'];

    foreach ($_SESSION['cart_item']as $product){
        $id=$product['id'];
        $qty=$product['quantity'];
        $sql="insert into guestproducts (orderid, productid, quantity)
        values ($orderid,$id, $qty)";
        $con->exec($sql);

    }
}

require 'views/Bestellen.view.php';

