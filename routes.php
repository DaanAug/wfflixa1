<?php

#routes for all visitors/users
$routesAll = [
    '' => 'controllers/home.php',
    'home' => 'controllers/home.php',
    'contact' => 'controllers/contact.php',
    'videos' => 'controllers/videos.php',
    'courses' => 'controllers/courses.php',
    'login' => 'controllers/login.php',
    'registration' =>'controllers/registration.php',
    'style' => 'views/styles/style.css',
    'dashboard' => 'views/dashboard.php',
    'Winkelmand' => 'controllers/Winkelmand.php',
    'Bestellen' => 'controllers/Bestellen.php',
    'Bedankt' => 'controllers/Bedankt.php',
    'Betalen' => 'controllers/Betalen.php',
    'videopage' => 'controllers/videopage.php',
    'coursepage' => 'controllers/coursepage.php',
    'addCourse' => 'controllers/addCourse.php',
    'upload' => 'controllers/uploadVideo.php'

];


#routes for logged in users
$routesUsr = [
    'logout' => 'controllers/logout.php',
    'profilepage' => 'controllers/profilepage.php',
    'upload' => 'controllers/uploadVideo.php'


];


#routes for admins
$routesAdm = [
    'admin' => 'controllers/admin.php',
    'addCourse' => 'controllers/addCourse.php',
    'editCourse' => 'controllers/adminEditCourse.php',
    'edtCurrProduct' => 'controllers/edtCurrProduct.php',
    'dltCurrCourse' => 'controllers/dltCurrCourse.php',
    'userspage' => 'controllers/userspage.php',
    'edituser' => 'controllers/edituser.php',
    'orders' => 'controllers/orders.php',
    'orderproducts' => 'controllers/orderproducts.php'


];

$mergeUsr = array_merge($routesAll,$routesUsr);
$mergeAdm = array_merge($routesAll,$routesUsr,$routesAdm);

#if user is logged in
if(session_status() === PHP_SESSION_NONE) {
session_start();
}

if(isset($_SESSION['isadmin']))
    {
        #If user is admin
        if($_SESSION['isadmin']== true)
        {
            $router->define($mergeAdm);
        }
        #If user is regular customer
        elseif ($_SESSION['isadmin']== false)
        {
            $router->define($mergeUsr);
        }
        else
        {
            $router->define($routesAll);
        }


    }
#If not logged in
else
{
    $router->define($routesAll);
}

