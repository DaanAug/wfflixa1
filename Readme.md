#WFFLIX

user accounts:

|  Type     |Gebruikersnaam   |Wachtwoord   |
|:---------:|:---------------:|:-----------:|
| admin     |admin            |admin1!      |
| teacher   |teacher          |teacher1!    |
| gebruiker |student          |student1!    |



Database dump:
```mariadb
CREATE database wfflixa1;

create table courses
(
courseID    int auto_increment
primary key,
title       varchar(255)                         null,
description varchar(255)                         null,
createdAt   datetime default current_timestamp() null,
updatedAt   datetime default current_timestamp() null
);

create table users
(
userID    int auto_increment
primary key,
username  varchar(128)                         not null,
password  varchar(128)                         not null,
email     varchar(255)                         null,
usertype  varchar(32)                          null,
createdAt datetime default current_timestamp() null,
updatedAt datetime default current_timestamp() null
);

create table teachercourses
(
id        int auto_increment
primary key,
courseID  int                                  not null,
teacherID int                                  not null,
createdAt datetime default current_timestamp() null,
updatedAt datetime default current_timestamp() null,
constraint teachercourses_ibfk_1
foreign key (courseID) references courses (courseID),
constraint teachercourses_ibfk_2
foreign key (teacherID) references users (userID)
);

create index courseID
on teachercourses (courseID);

create index teacherID
on teachercourses (teacherID);

create table videos
(
videoID     int auto_increment
primary key,
title       varchar(255)                         null,
description varchar(255)                         null,
uploadedBy  int                                  null,
category    varchar(255)                         null,
course      int                                  null,
videoLink   varchar(512)                         null,
likes       int      default 0                   null,
dislikes    int      default 0                   null,
createdAt   datetime default current_timestamp() null,
updatedAt   datetime default current_timestamp() null,
constraint videos_ibfk_1
foreign key (uploadedBy) references users (userID),
constraint videos_ibfk_2
foreign key (course) references courses (courseID)
);

create table comments
(
commentID   int auto_increment
primary key,
videoID     int                                  not null,
placedBy    int                                  not null,
commentText varchar(1024)                        null,
likes       int      default 0                   null,
dislikes    int      default 0                   null,
createdAt   datetime default current_timestamp() null,
updatedAt   datetime default current_timestamp() null,
constraint comments_ibfk_1
foreign key (videoID) references videos (videoID),
constraint comments_ibfk_2
foreign key (placedBy) references users (userID)
);

create index placedBy
on comments (placedBy);

create index videoID
on comments (videoID);

create table likedcomments
(
id        int auto_increment
primary key,
userID    int                                    null,
commentID int                                    null,
liked     tinyint(1) default 0                   null,
disliked  tinyint(1) default 0                   null,
createdAt datetime   default current_timestamp() null,
updatedAt datetime   default current_timestamp() null,
constraint likedcomments_ibfk_1
foreign key (userID) references users (userID),
constraint likedcomments_ibfk_2
foreign key (commentID) references comments (commentID)
);

create index commentID
on likedcomments (commentID);

create index userID
on likedcomments (userID);

create table likedvideos
(
id        int auto_increment
primary key,
userID    int                                    null,
videoID   int                                    null,
liked     tinyint(1) default 0                   null,
disliked  tinyint(1) default 0                   null,
createdAt datetime   default current_timestamp() null,
updatedAt datetime   default current_timestamp() null,
constraint likedvideos_ibfk_1
foreign key (userID) references users (userID),
constraint likedvideos_ibfk_2
foreign key (videoID) references videos (videoID)
);

create index userID
on likedvideos (userID);

create index videoID
on likedvideos (videoID);

create index course
on videos (course);

create index uploadedBy
on videos (uploadedBy);


insert into courses (courseID, title, description, createdAt, updatedAt)
values  (1, 'testcourse', 'test description', '2021-10-25 23:15:14', '2021-10-25 23:15:14'),
(2, 'php', 'phptest', '2021-10-28 14:20:34', '2021-10-28 14:20:34'),
(5, 'Bootstrap', 'Bootstrap cursus', '2021-10-29 10:30:16', '2021-10-29 10:30:16'),
(6, 'DOMP', 'DOMP cursus', '2021-10-30 09:59:35', '2021-10-30 09:59:35');



insert into users (userID, username, password, email, usertype, createdAt, updatedAt)
values  (1, 'teststudent', 'bae610bc3370ac41970dbcf7ea270015', 'student@student.nl', '0', '2021-10-04 22:10:58', '2021-10-04 22:10:58'),
(2, 'testteacher', '526dded03c9e0cb3e2c3e0fb8f419360', 'teacher@teacher.nl', '1', '2021-10-04 22:10:58', '2021-10-04 22:10:58'),
(4, '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', null, '2021-10-26 10:00:06', '2021-10-26 10:00:06'),
(5, 't', 'e358efa489f58062f10dd7316b65649e', 't', null, '2021-10-26 10:00:18', '2021-10-26 10:00:18'),
(6, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test@test.nl', null, '2021-10-27 18:49:31', '2021-10-27 18:49:31'),
(8, 'admin', '8cb08622a46d5800f0f332fb55b8c553', 'admin@admin.nl', '2', '2021-10-28 13:13:07', '2021-10-28 13:13:07'),
(9, 'teacher', '526dded03c9e0cb3e2c3e0fb8f419360', 'teacher@teacher.nl', '1', '2021-10-28 13:13:07', '2021-10-28 13:13:07'),
(10, 'student', 'cd73502828457d15655bbd7a63fb0bc8', 'student@student.nl', '0', '2021-10-28 13:13:07', '2021-10-28 13:13:07');

insert into videos (videoID, title, description, uploadedBy, category, course, videoLink, likes, dislikes, createdAt, updatedAt)
values  (1, 'testvideo', 'test description', 8, '1', 1, 'public/img/file_example_MP4_480_1_5MG - Copy.mp4', 0, 0, '2021-10-25 23:23:14', '2021-10-25 23:23:14'),
(2, 'DOMP PDO', 'PDO() Database connection
', 8, '1', 6, 'public/img/DOMP PDO.mp4', 0, 0, '2021-10-29 09:16:00', '2021-10-29 09:16:00'),
(3, 'DOMP View', 'Video over views', 8, '1', 6, 'public/img/DOMP VIEW.mp4', 0, 0, '2021-10-29 10:15:57', '2021-10-29 10:15:57'),
(4, 'Domp Associative array', 'Associative array', 8, '1', 6, 'public/img/DOMP AssociativeArray.mp4', 0, 0, '2021-10-29 10:17:57', '2021-10-29 10:17:57'),
(5, 'Domp config', 'Configuration file', 8, '1', 6, 'public/img/DOMP config.mp4', 0, 0, '2021-10-29 10:19:26', '2021-10-29 10:19:26');

insert into comments (commentID, videoID, placedBy, commentText, likes, dislikes, createdAt, updatedAt)
values  (2, 1, 10, 'testcomment', 0, 0, '2021-10-30 11:07:38', '2021-10-30 11:07:38'),
(3, 1, 10, 'testcomment23', 0, 0, '2021-10-30 11:08:02', '2021-10-30 11:08:02');
```