<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>
<form method="post" name="editproduct" enctype="multipart/form-data">


    <table class="table">
        <thead>
            <tr>
                <th>Afbeelding</th>
                <th>Productnaam</th>
                <th>Beschrijving</th>
                <th>Prijs</th>
                <th>Beschikbaar</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><input type="file" name="fileToUpload" id="fileToUpload"></td>
                <td><input type="text" name="productname"  placeholder="Productnaam" ></td>
                <td><textarea name="description" rows="4" cols="50" placeholder="Productnaam" >    </textarea></td>
                <td><input type="number" name="price" step="0.01"  placeholder="Prijs" ></td>
                <td><input type="checkbox" name="isavailable" value="available" ></td>
            </tr>
    </table>

    <input type="submit" value="Update" name="submit">
    </form>