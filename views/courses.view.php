<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>

    <link rel="stylesheet" type="text/css" href="public/css/course.css">
    <link rel="stylesheet" type="text/css" href="public/css/Navbar.css">
    <title>Cursussen</title>

    <div class="container">
        <div class="row justify-content-center">
            <nav class="navbar">
                <div class="container-fluid">
                    <form action="courses" method="GET">
                        <div class="col-sm-3 ms-auto"><input id="search" class="form-control" name="search" type="text" placeholder="Zoek hier op cursusnaam"></div>
                        <div class="col-sm-2 ms-auto"><input id="submit" class="form-control" type="submit" value="Zoeken"></div>
                    </form>
                </div>
            </nav>
        </div>


        <div class="row justify-content-center">

    <div class="no-records"><h3 style="color:whitesmoke">Kies een van de cursussen die wij bieden.</h3></div>

            <?php
            //This loop is counting the amount of items within the $sappen array.
            //Then for each item within the array it writes the html code below.
            foreach ($courses as $course) {
                // Check if the item is allowed to be shown
                //if ($sap["isavailible"] == 1){?>
                <div class="col-md-3 ms-auto" style="border: 2px white solid;" >
                            <div class="Course">
                                <p class="card-text"><?php echo $course["title"] ?></a></p>
                                <p class="card-text"><?php echo $course["description"] ?></p>
                                <a href="coursepage?id=<?php echo $course["courseID"] ?>" class="btn btn-success"><span class="glyphicon glyphicon-arrow-right"></span>Meer lezen</a>
                            </div>

                </div>
            <?php }//} ?>
        </div>
    </div>
<?php include('views/Base/Footer.php') ?>