<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Login</title>
    <link rel="stylesheet" href="public/css/style.css"/>
</head>
<body>
<div class="login-title" style="color:whitesmoke">
    Inloggen
</div>

<form class="form" method="post" name="login">
    <input type="text" class="login-input form-control" name="username" placeholder="Gebruikersnaam of email adress" autofocus="true" required/>
    <input type="password" class="login-input form-control" name="password" placeholder="Wachtwoord" required/>
    <input type="submit" value="Login" name="submit" class="login-button"/>
    <a class="forgotPassword">Wachtwoord vergeten?</a>
</form>

</body>
</html>