
<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>


<title>Videos</title>
<head>


    <link rel="stylesheet" type="text/css" href="public/css/Products.css">
    <link rel="stylesheet" type="text/css" href="public/css/Navbar.css">


</head>
<div class="container">
    <div class="row justify-content-center">
        <nav class="navbar">
            <div class="container-fluid">
                <form action="videos" method="GET">
                    <div class="col-sm-3 ms-auto"><input id="search" class="form-control" name="search" type="text" placeholder="Zoek hier op video"></div>
                    <div class="col-sm-2 ms-auto"><input id="submit" class="form-control" type="submit" value="Zoeken"></div>
                </form>
            </div>
        </nav>
    </div>

    <div class="row row-cols-1 row-cols-md-2 g-4">

        <?php
        //This loop is counting the amount of items within the $sappen array.
        //Then for each item within the array it writes the html code below.
        foreach ($videos as $video) {
            // Check if the item is allowed to be shown
            $date = date_create($video[10]);
            $cid = $video["course"];

            $coursesql = "SELECT title FROM courses WHERE courseID = $cid";
            $courseres = $con->query($coursesql);
            $courseres = $courseres->fetch();


            ?>


                <div class="col-md-3">
                    <a href="videopage?id=<?php echo $video["videoID"] ?>">
                        <div class="card">
                            <video src="<?=$video['videoLink']?>"></video>
                            <div class="card-body">
                                <h3 class="card-title"><?= $video['title']?></h3>
                                <p class="card-text"><?= $video['description']?></p>
                                <p class="card-text"> Course: <?= $courseres['title']?></p>
                                <p class="card-text"> Geüpload door: <?= $video['username']?></p>
                                <p class="card-footer"><?= date_format($date,'d/m/Y') ?></p>

                            </div>
                        </div>
                    </a>
                </div>


        <?php }//} ?>
    </div>
</div>

<?php include('views/Base/Footer.php') ?>