<?php
include("Base/Header.php");
include("Base/Navbar.php");
?>
<link rel="stylesheet" type="text/css" href="public/css/userspage.css" xmlns="http://www.w3.org/1999/html">

<div class="container">

    <div class="row justify-content-center">
        <nav class="navbar">
            <div class="container-fluid">
                <form action="userspage" method="GET">
                    <div class="col-sm-3 ms-auto"><input id="search" class="form-control" name="search" type="text" placeholder="Zoek hier op gebruikersnaam"></div>
                    <div class="col-sm-2 ms-auto"><input id="submit" class="form-control" type="submit" value="Zoeken"></div>
                </form>
            </div>
        </nav>
    </div>

    <div class="row" style="padding: 10px">
        <div class="col-sm-2 header"> Gebruikersnaam</div>
        <div class="col-sm-2 header"> Email</div>
        <div class="col-sm-2 header"> Adres </div>
        <div class="col-sm-2 header"> Postcode </div>
        <div class="col-sm-2 header"> Telefoonnummer </div>
        <div class="col-sm-2 header"> Acties </div>
    </div>

<?php foreach ($users as $user){?>

    <div class="row" style="padding: 10px">
        <div class="col-sm-2 data">
            <div class="inner"><?php echo $user['username'];?></div>
        </div>
        <div class="col-sm-2 data">
            <div class="inner"><?php echo $user['email'];?> </div>
        </div>
        <div class="col-sm-2 data">
            <div class="inner"><?php echo $user['address'];?></div>
        </div>
        <div class="col-sm-2 data">
            <div class="inner"><?php echo $user['zipcode'];?></div>
        </div>
        <div class="col-sm-2 data">
            <div class="inner"><?php echo $user['phone'];?></div>
        </div>
        <div class="col-sm-1 data">
            <div class="inner">
                <div class="inner">
                    <input name="id" type="text" hidden value="<?php echo $user['userid'];?>">
                    <a href="edituser?id=<?php echo $user['userid'];?>"><button class="form-control glyphicon glyphicon-edit" value="Edit"></button></a>
                </div>
            </div>
        </div>
        <form method="post">
        <div class="col-sm-1 data">
            <div class="inner">
                <input name="id" type="text" hidden value="<?php echo $user['userid'];?>">
                <input name="<?php echo $user['userid'];?>" class="form-control glyphicon glyphicon-remove" type="submit" value="Delete">
            </div>
        </div>
        </form>
    </div>

<?php } ?>
</div>


<?php include("Base/Footer.php"); ?>

