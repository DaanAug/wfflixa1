<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>

<?php foreach ($users as $user){
    $_SESSION["id"] = $user["id"];
    ?>

    <form method="post">
<div class="container">
    <div class="container" style="width: 50% !important;">
    <div style="padding: 5px">
        <p>Username</p>
        <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $user['username'];?>">
    </div>
    <div style="padding: 5px">
        <p>Email</p>
        <input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo $user['email'];?>">
    </div>
    <div style="padding: 5px">
        <p>Adres</p>
        <input type="text" class="form-control" placeholder="Adres" name="adress" value="<?php echo $user['address'];?>">
    </div>
    <div style="padding: 5px">
        <p>Postcode</p>
        <input type="text" class="form-control" placeholder="Postcode" name="zipcode" value="<?php echo $user['zipcode'];?>">
    </div>
    <div style="padding: 5px">
        <p>Telefoonnummer</p>
        <input type="text" class="form-control" placeholder="Telefoonnummer" name="phonenumber" value="<?php echo $user['phone'];?>">
    </div>
    <div style="padding: 5px">
        <input type="submit" class="form-control" value="Wijzig Gegevens" name="wijzigen">
    </div>
    </div>
</div>
    </form>
<?php }?>


<?php include('views/Base/Footer.php') ?>
