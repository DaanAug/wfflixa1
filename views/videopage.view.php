<?php include "Base/Header.php";?>
<?php include "Base/Navbar.php";?>
<?php
// The while loop is contantly being used a long as there is data to fetch
while($row = $result->fetch()) {
    $videos[] = $row;
}
?>

<div class="container">
    <?php foreach ($videos as $video) {
        $date = date_create($video[10]);
        $cid = $video["course"];
        $coursesql = "SELECT title FROM courses WHERE courseID = $cid";
        $courseres = $con->query($coursesql);
        $courseres = $courseres->fetch();?>

    <div class="row">
        <div class="col-sm-2">
            <button class="form-control" onclick="Back()">Terug</button>
        </div>
    </div>
        <br>

    <div class="row justify-content-center">
        <div class="col-md-12">

                <div class="card">
                    <video width="100%" height="100%" controls id="video" preload="metadata">
                        <source src="<?php echo $video["videoLink"] ?>" type="video/mp4">
                    </video>
                    <div class="card-body">
                        <h3 style="color:whitesmoke" class="card-title"><?= $video['title']?></h3>
                        <p style="color:whitesmoke" class="card-text"><?= $video['description']?></p>
                        <p style="color:whitesmoke" class="card-text"> Course: <?= $courseres['title']?></p>
                        <p style="color:whitesmoke" class="card-text"> Geüpload door: <?= $video['username']?></p>
                        <p style="color:whitesmoke" class="card-footer"><?= date_format($date,'d/m/Y') ?></p>

                    </div>
                </div>

        </div>




    </div>



    <?php
    }
    ?>
</div>


<script src="public/js/Productpage.js"></script>

<?php include "Base/Footer.php";?>
