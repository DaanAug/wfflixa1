<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php')?>
<?php
if (!isset($_SESSION['cart_item']) || empty($_SESSION['cart_item'])) {
    header('location:Winkelmand');
    exit();
}
?>
    <div class="row mt-3">
    <div class="col-md-4 order-md-2 mb-4">
    <h4 class="d-flex justify-content-between align-items-center mb-3">
        <span class="text-muted">Uw winkelmand</span>
    </h4>
    <ul class="list-group mb-3">
<?php
$total = 0;
foreach($_SESSION['cart_item'] as $cartItem)
{
    $total+=$cartItem["quantity"]*$cartItem["price"];
    ?>
    <li class="list-group-item d-flex justify-content-between lh-condensed">
        <div>
            <h6 class="my-0"><?php echo $cartItem['productname'] ?></h6>
            <small class="text-muted">Aantal: <?php echo $cartItem['quantity'] ?> X Price: <?php echo $cartItem['price'] ?></small>
        </div>
        <span class="text-muted">€<?php echo $cartItem["quantity"]*$cartItem["price"] ?></span>
    </li>
    <?php
}
?>
        <li class="list-group-item d-flex justify-content-between">
            <span>Totaal (EUR)</span>
            <strong>€<?php echo number_format($total,2);?></strong>
        </li>
    </ul>
        <a href="Winkelmand" class="btn btn-success"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Wijzig winkelmand</a>
    </div>
<?php

require_once('models/db.php');
$cartItemCount = count($_SESSION['cart_item']);

        ?>

    <link href="netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="public/css/Bestellen.css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Gegevens</h4>
            <?php
            if(isset($errorMsg) && count($errorMsg) > 0)
            {
                foreach($errorMsg as $error)
                {
                    echo '<div class="alert alert-danger">'.$error.'</div>';
                }
            }
            ?>

            <?php
            if (!isset($_SESSION['username'])){


            ?>

            <form class="needs-validation" method="POST">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">Voornaam</label>
                        <input type="text" class="form-control" id="firstName" name="firstname" placeholder="Voornaam" value="<?php echo (isset($fnameValue) && !empty($fnameValue)) ? $fnameValue:'' ?>" >
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Achternaam</label>
                        <input type="text" class="form-control" id="lastName" name="lastname" placeholder="Achternaam" value="<?php echo (isset($lnameValue) && !empty($lnameValue)) ? $lnameValue:'' ?>" >
                    </div>
                </div>

                <div class="mb-3">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="you@example.com" value="<?php echo (isset($emailValue) && !empty($emailValue)) ? $emailValue:'' ?>">
                </div>

                <div class="mb-3">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Astraat" value="<?php echo (isset($addressValue) && !empty($addressValue)) ? $addressValue:'' ?>">
                </div>
                    <div class="mb-3">
                        <label for="zip">Postcode</label>
                        <input type="text" class="form-control" id="zip" name="zipcode" placeholder="" value="<?php echo (isset($zipCodeValue) && !empty($zipCodeValue)) ? $zipCodeValue:'' ?>" >
                    </div>

                <button class="btn btn-primary btn-lg btn-block" type="submit" name="orderguest" value="orderguest">Bestelling plaatsen</button>
            </form>
                <?php
            }

            else{
                $username = $_SESSION['username'];
                $sql= "
                SELECT *
                FROM profile
                INNER JOIN users
                ON profile.userid = users.id
                WHERE username = '$username'";
                $result = $con->query($sql);
                $result=$result->fetch();
//                var_dump($result);
                ?>

                <form class="needs-validation" method="POST">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">Gebruikersnaam</label>
                            <input type="text" class="form-control" id="firstName" name="first_name" placeholder="Voornaam" value="<?php echo $result['username'] ?>">
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" value="<?php echo $result['email'] ?>">
                    </div>

                    <div class="mb-3">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder="Astraat" value="<?php echo $result['address'] ?>">
                    </div>
                    <div class="mb-3">
                        <label for="zip">Postcode</label>
                        <input type="text" class="form-control" id="zip" name="zipcode" placeholder="" value="<?php echo $result['zipcode'] ?>" >
                    </div>
                    <div class="mb-3">
                        <label for="zip">id</label>
                        <input type="text" class="form-control" id="id" name="id" placeholder="" value="<?php echo $result['id'] ?>" >
                    </div>

                    <button class="btn btn-primary btn-lg btn-block" type="submit" name="orderacc" value="orderacc">Bestelling plaatsen</button>
                </form>

                <?php
            }
            ?>
        </div>
    </div>


<?php
