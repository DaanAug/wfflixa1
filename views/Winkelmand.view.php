<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php')?>
<?php
//
//// The while loop is contantly being used a long as there is data to fetch
//while($row = $result->fetch()) {
//    $sappen[] = $row;
//}
//
//?>


<?php
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
require_once("models/db.php");
if(!empty($_GET["action"])) {
switch($_GET["action"]) {
case "add":
if(!empty($_POST["quantity"])) {
$productByCode = $con->Query("SELECT * FROM products WHERE id='" . $_GET["id"] . "'");
$productByCode = $productByCode->fetchAll();
$itemArray = array($productByCode[0]["id"]=>array('productname'=>$productByCode[0]["productname"], 'id'=>$productByCode[0]["id"], 'quantity'=>$_POST["quantity"], 'price'=>$productByCode[0]["price"], 'productimagepath'=>$productByCode[0]["productimagepath"], 'description'=>$productByCode[0]["description"]));

if(!empty($_SESSION["cart_item"])) {
if(in_array($productByCode[0]["id"],array_keys($_SESSION["cart_item"]))) {
foreach($_SESSION["cart_item"] as $k => $v) {
if($productByCode[0]["id"] == $k) {
if(empty($_SESSION["cart_item"][$k]["quantity"])) {
$_SESSION["cart_item"][$k]["quantity"] = 0;
}
$_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
}
}
} else {
$_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
}
} else {
$_SESSION["cart_item"] = $itemArray;
}
}
break;
case "remove":
if(!empty($_SESSION["cart_item"])) {
foreach($_SESSION["cart_item"] as $k => $v) {
if($_GET["code"] == $k)
unset($_SESSION["cart_item"][$k]);
if(empty($_SESSION["cart_item"]))
unset($_SESSION["cart_item"]);
}
}
break;
case "empty":
unset($_SESSION["cart_item"]);
break;
}
}
?>


<link rel="stylesheet" type="text/css" href="netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="public/css/Winkelmand.css">

<?php //foreach ($sappen as $sap) {?>
<form action="/Winkelmand?page=cart" method="post">
<div class="panel-body">
    <div class="table-responsive">
        <a id="btnEmpty" href="Winkelmand?action=empty">Empty Cart</a>
        <?php
        if(isset($_SESSION["cart_item"])){
            $total_quantity = 0;
            $total_price = 0;
            ?>
            <table class="table">
            <thead>
            <tr>
                <th></th>
                <th>Productnaam</th>
                <th>Beschrijving</th>
                <th>Aantal</th>
                <th>Prijs</th>
                <th>Totaal</th>
            </tr>
            </thead>
            <tbody>
                <?php
    foreach ($_SESSION["cart_item"] as $sap){
        $item_price = $sap["quantity"]*$sap["price"];
		?>

            <tr>
                <td><img src=<?php echo $sap["productimagepath"] ; ?> width="100px"></td>
                <td><strong><p><?php echo $sap["productname"] ;?></p></strong></td>
                <td><strong><p><?php echo $sap["description"] ;?></p></strong></td>
                <td>
                    <form class="">
                        <div class="cart-action"><input type="text" class="product-quantity" name="quantity" value=<?php echo $sap["quantity"] ; ?>>
                        <a href="Winkelmand?action=remove&id=<?php echo $sap["id"]; ?>" class="btn btn-primary"><i class="fa fa-trash-o"></i></a>
                    </form>
                </td>
                <td>€ <?php echo $sap["price"] ?></td>
                <td>€ <?php echo $sap["quantity"]*$sap["price"] ?></td>
                <td></td>
            </tr>

          <?php
				$total_quantity += $sap["quantity"];
				$total_price += ($sap["price"]*$sap["quantity"]);
		}
		?>
<td colspan="2" align="right">Subtotaal:</td>
<!--<td align="right">--><?php //echo $total_quantity; ?><!--</td>-->
<td align="right" colspan="2"><strong><?php echo "€ ".number_format($total_price, 2); ?></strong></td>
<td></td>
            </tr>
            </tbody>
        </table>
          <?php
} else {
?>
            <div class="no-records"><h3>Je winkelwagentje is leeg.</h3><br>Had je er wel iets in gelegd?</div>
<?php
}
?>
    </div>
</form>
</div>
</div>
<a href="Products" class="btn btn-success"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Verder winkelen</a>
<a href="Bestellen" class="btn btn-primary pull-right">Bestellen<span class="glyphicon glyphicon-chevron-right"></span></a>
</div>
<?php  ?>
</div>
</div>
</div>

