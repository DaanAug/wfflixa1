<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Registration</title>
    <link rel="stylesheet" href="public/css/style.css"/>

</head>
<body>
<div class="login-title">
    Registreren
</div>
<form class="form" action="" method="post">
    <input type="text" class="login-input form-control" name="username" placeholder="Gebruikersnaam" required>
    <input type="text" class="login-input form-control" name="email" placeholder="Email Adres" required>
    <input type="password" class="login-input form-control" name="password" placeholder="Wachtwoord" required>
    <input type="text" class="login-input form-control" name="address" placeholder="Adres" required>
    <input type="text" class="login-input form-control" name="zipcode" placeholder="Postcode" pattern="[1-9][0-9]{3}\s?[a-zA-Z]{2}" title="4 cijfers[0-9] 2 letters[A-Z]" maxlength="7" required>
    <input type="text" class="login-input form-control" name="phone" placeholder="Telefoon nr." pattern="[0-9]*" title="Alleen getallen [0-9]" required>
    <input type="submit" name="submit" value="Register" class="login-button">
</form>
</body>
</html>