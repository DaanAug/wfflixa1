
<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>


<!--<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"-->
<!--      integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">-->
<!---->
<!--<meta charset="UTF-8">-->
<!--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"-->
<!--        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"-->
<!--        crossorigin="anonymous"></script>-->
<link rel="stylesheet" href="public/css/Admin.css"/>




<?php

// The while loop is contantly being used a long as there is data to fetch
while($row = $result->fetch()) {
    $courses[] = $row;
}
?>
    <style>
        table, th, td {
            border:1px solid black;
            text-align: center;
            vertical-align: bottom;
        }

    </style>
<div class="container" style="color:whitesmoke">
    <table class="table">
        <thead>
        <tr>

            <th>Cursus</th>
            <th>Beschrijving</th>
            <th></th>

        </tr>
        </thead>
        <tbody>



<?php
//This loop is counting the amount of items within the $sappen array.
//Then for each item within the array it writes the html code below.
foreach ($courses as $course) {?>

<tr>

    <td><p><?= $course["title"] ?></p></td>
    <td><p><?= $course["description"] ?></p></td>
    <td><a href="dltCurrProduct?id=<?php echo $course['courseID']; ?>" onclick="return confirm('Wil je dit product definitief verwijderen? \n Dit kan niet ongedaan worden gemaakt!');"><b><span class="glyphicon glyphicon-trash"></span> Delete</b></a></td>



</tr>

<?php } ?>
</tbody>
    </table>

</div>


