<head>
    <link href="public/css/footer.css" rel="stylesheet" />
</head>

<br>
<br>
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 text-center">
                <img src="views/img/WFFLIX logo.png">
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Contact</h6>
                <ul class="footer-links">
                    <li><p>WFFLIX bv</p></li>
                    <li><p>Hospitaaldreef 5</p></li>
                    <li><p>1315 RC Almere, Nederland</p></li>
                    <li><p>Tel: 088 469 6600</p></li>
                    <li><p>info@windesheim.nl</p></li>
                </ul>
            </div>

            <div class="col-xs-6 col-md-3 text-center">
                <ul class="footer-links">
                    <br>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">

            </div>
        </div>
    </div>
</footer>