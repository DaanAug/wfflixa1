<link rel="stylesheet" type="text/css" href="public/css/Navbar.css">

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li><a href="home"><b>Home</b></a></li>
            <li><a href="videos"><b>Videos</b></a></li>
            <li><a href="courses"><b>Cursussen</b></a></li>
            <li><a href="upload"><b>Uploaden</b></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php if (session_status() === PHP_SESSION_NONE) {
                session_start();
            }
             if(isset($_SESSION["username"])){?>
                     <?php if ($_SESSION['isadmin'] == true){ ?>

                     <li><a href="admin"><b><span class="glyphicon glyphicon-edit"></span> Admin Pagina</b></a></li>
                     <?php } ?>
                    <li><a href="profilepage"><b><span class="glyphicon glyphicon-user"></span> <?= $_SESSION["username"]; ?></b></a></li>
                    <li><a href="logout"><b><span class="glyphicon glyphicon-log-out"></span> Log Out</b></a></li>
            <?php } else {?>
            <li><a href="registration"><b><span class="glyphicon glyphicon-user"></span> Sign Up</b></a></li>
            <li><a href="login"><b><span class="glyphicon glyphicon-log-in"></span> Login</b></a></li>
            <?php }?>



        </ul>
    </div>
</nav>