<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>
<link rel="stylesheet" href="public/css/Admin.css"/>

<?php if(isset($result)){?>
    <style>
        table, th, td {
            border:1px solid black;
            text-align: center;
            vertical-align: bottom;
        }

    </style>
    <div class="container">
        <h3></h3>
        <form method="post" name="editproduct" enctype="multipart/form-data">


            <!--    <table class="table">-->
            <!--        <thead>-->
            <!--            <tr>-->
            <!--                <th>Afbeelding</th>-->
            <!--                <th>Productnaam</th>-->
            <!--                <th>Beschrijving</th>-->
            <!--                <th>Prijs</th>-->
            <!--                <th>Beschikbaar</th>-->
            <!--            </tr>-->
            <!--        </thead>-->
            <!--        <tbody>-->
            <!--            <tr>-->
            <!--                <td><img width="50" src="--><?php //echo $result["productimagepath"] ?><!--"><input type="file" name="fileToUpload" id="fileToUpload"></td>-->
            <!--                <td><input type="text" name="productname" value="--><?php //echo $result["productname"] ?><!--"    placeholder="Productnaam" Required></td>-->
            <!--                <td><textarea name="description" rows="4" cols="50" placeholder="Productnaam" Required>--><?php //echo $result["description"] ?><!--    </textarea></td>-->
            <!--                <td><input type="number" name="price" step="0.01" value="--><?php //echo $result["price"] ?><!--"    placeholder="Prijs" Required></td>-->
            <!--                <td><input type="checkbox" name="isavailable" value="available" --><?php //echo ($result["isavailible"]==1) ? 'Checked' : '';?><!-- ></td>-->
            <!--            </tr>-->
            <!--    </table>-->
            <!---->
            <!--    <input type="submit" value="Update" name="submit">-->


            <div class="container">
                <div class="col-md-9 ms-auto adminProducts">
                    <div class="row adminAddProduct">
                        <div class="adminAddProductTitle">
                            Update <?php echo $result["productname"] ?>
                        </div>

                        <div class="col-md-6 adminAddProductInputs">


                            <div style="padding: 8px">
                                <p><b>Productnaam</b></p>
                                <input type="text" name="productname" value="<?php echo $result["productname"] ?>" placeholder="Productnaam" Required>
                            </div>
                            <div style="padding: 8px">
                                <p><b>Beschrijving</b></p>
                                <textarea name="description" rows="5" cols="25" placeholder="Productnaam" Required><?php echo $result["description"] ?>    </textarea>
                            </div>
                            <div style="padding: 8px">
                                <p><b>Prijs</b></p>
                                <input type="number" name="price" step="0.01" value="<?php echo $result["price"] ?>"    placeholder="Prijs" Required>
                            </div>
                            <div style="padding: 8px">
                                <p><b>Beschikbaar</b></p>
                                <input type="checkbox" name="isavailable" value="available" <?php echo ($result["isavailible"]==1) ? 'Checked' : '';?> >
                            </div>
                        </div>
                        <div class="col-md-6">


                            <div class="custom-file mt-3 mb-3">
                                <p><b>Afbeelding</b></p>
                                <div style="padding: 8px"id="display_image">

                                    <img width="200" src="<?php echo $result["productimagepath"] ?>">
                                    <input type="file" name="fileToUpload" id="fileToUpload">
                                </div>
                            </div>
                            <br>

                            <input type="submit" class="adminAddProductUpload" value="Update" name="submit">
                        </div>
                    </div>
                </div>
            </div>
    </div>









    </form>
    </div>
<?php } ?>