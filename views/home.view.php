
<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>

<title>Home</title>
<body>
<head>
    <link href="public/css/home.css" rel="stylesheet">
</head>

<div class="text-center">
    <h1 class="hometitle">Welkom bij WFFLIX!</h1>
    <br>
    <br>
</div>

<div class="slideshow-container">

    <div class="mySlides fade1">
        <div class="numbertext">1 / 3</div>
        <video <video onclick="location.href='videopage?id=4'" src="public/img/DOMP AssociativeArray.mp4" style="width:100%"></video>
        <div class="text">Domp Associative array</div>
    </div>

    <div class="mySlides fade1">
        <div class="numbertext">2 / 3</div>
        <video onclick="location.href='videopage?id=1'" src="public/img/file_example_MP4_480_1_5MG - Copy.mp4" style="width:100%"></video>
        <div class="text">Testvideo</div>
    </div>

    <div class="mySlides fade1">
        <div class="numbertext">3 / 3</div>
        <video <video onclick="location.href='videopage?id=3'" src="public/img/DOMP VIEW.mp4" style="width:100%"></video>
        <div class="text">DOMP VIEW</div>
    </div>
    <!-- Next and previous buttons -->
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
    <span class="dot" onclick="currentSlide(1)"></span>
    <span class="dot" onclick="currentSlide(2)"></span>
    <span class="dot" onclick="currentSlide(3)"></span>
</div>

<br>
<br>

<div class="container">
    <br>
    <br>
    <div class="row">
        <div class="col-md-6 text-center">
            <img src="views/img/homepage-img1.jpg" class="homepageimg">
        </div>
        <div class="col-md-6 text-center hometext" id="scroll">
            <h1>Netflix voor Windesheim Flevoland</h1>
            WFFLIX is een platform waar mensen met de interesse om te programmeren dit kunnen leren. Met WFFLIX willen wij ervoor zorgen dat je als student gemakkelijk video's kan vinden en bekijken over programmeren.
            <br>
            <br>
            <input onclick="location.href='videos'" type="button" class="homepagebutton" value="Klik hier om de videos te bekijken!">
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="col-md-12" >
        <div class="col-md-6  text-center hometext" id="scroll">
            <h1>Deel je kennis!</h1>
            Niet alleen docenten kunnen hun videos uploaden maar studenten ook! Als je zelf je kennis wilt delen met andere studenten kan dat hier door je eigen video te uploaden op deze site.
            <br>
            <br>
            <div class="text-center">
                <input onclick="location.href='registration'" type="button" class="homepagebutton" value="Klik hier om te registreren!">
            </div>

        </div>
        <div class="col-md-6 text-center">
            <img src="views/img/homepage-img2.jpg" class="homepageimg">
        </div>

    </div>

</div>
<br>
<br>
<br>
<br>
<?php include('views/Base/Footer.php') ?>