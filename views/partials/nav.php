<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">WFFLIX</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link <?= (Request::uri()=== '')? 'active': ''; ?>" href="">Home <span class="sr-only"></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= (Request::uri()=== 'teachers')? 'active': ''; ?>" href="teachers">Teachers</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= (Request::uri()=== 'courses')? 'active': ''; ?>" href="courses">Courses</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= (Request::uri()=== 'contact')? 'active': ''; ?>" href="contact">Contact</a>
            </li>
        </ul>
    </div>
</nav>