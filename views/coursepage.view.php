<?php include "Base/Header.php";?>
<?php include "Base/Navbar.php";?>

<div class="container">
    <div style="margin:auto;width: 80%">
        <div class="row">
            <div class="col-sm-2">
                <button class="form-control" onclick="Back()";>Terug</button>
            </div>
        </div>

        <div class="row-md-4 text-center hometext" id="scroll">
            <h1 style="color:whitesmoke"><?= $result['title'] ?></h1>
            <h2 style="color:whitesmoke"><?= $result['description'] ?></h2>

        </div>

        <div class="row-md-4 text-center hometext" id="scroll" style="color:whitesmoke">

            <a href="videos" class="btn btn-success"><span class="glyphicon glyphicon-arrow-right"></span>Naar videos</a>
        </div>

        <script src="public/js/coursepage.js"></script>
    </div>
</div>
<?php include "Base/Footer.php";?>

