<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Registration</title>
    <link rel="stylesheet" href="style"/>
</head>
<body>
<div class='form'>
    <h3>You are registered successfully.</h3><br/>
    <p class='link'>Click here to <a href='login'>Login</a></p>
</div>
</body>
</html>