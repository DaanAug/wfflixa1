<?php
include("base/header.php");
include("base/navbar.php");
?>
<div class="container" style="width: 500px !important; border-top: 1px black solid; border-bottom: 1px black solid;">
    <h1>Edit</h1>
<?php foreach ($users as $user){?>
    <form method="post">
    <div class="row" style="padding: 10px">
        <div class="col data">
            <div class="inner">
                <input type="text" class="form-control" name="username" placeholder="Gebruikersnaam" value="<?php echo $user['username'];?>">
            </div>
        </div>
    </div>
        <div class="row" style="padding: 10px">
            <div class="col data">
                <div class="inner">
                    <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $user['email'];?>">
                </div>
            </div>
        </div>
        <div class="row" style="padding: 10px">
            <div class="col data">
                <div class="inner">
                    <input type="text" class="form-control" name="address" placeholder="Adres" value="<?php echo $user['address'];?>">
                </div>
            </div>
        </div>
        <div class="row" style="padding: 10px">
            <div class="col data">
                <div class="inner">
                    <input type="text" class="form-control" name="zipcode" placeholder="Postcode" value="<?php echo $user['zipcode'];?>">
                </div>
            </div>
        </div>
        <div class="row" style="padding: 10px">
            <div class="col data">
                <div class="inner">
                    <input type="text" class="form-control" name="phonenumber" placeholder="Telefoonnummer" value="<?php echo $user['phone'];?>">
                </div>
            </div>
        </div>
        <div class="row" style="padding: 10px">
            <div class="col data">
                <div class="inner">
                    <input name="id" type="text" hidden value="<?php echo $user['userid'];?>">
                    <input type="submit" class="form-control" name="Wijzigen" placeholder="submit" value="Wijzigen">
                </div>
            </div>
        </div>
        </form>
    </div>

<?php } ?>
</div>

<?php include("base/footer.php"); ?>
