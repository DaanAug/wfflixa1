<?php
include('base/header.php');
include('base/navbar.php');
?>

<link rel="stylesheet" type="text/css" href="public/css/userspage.css" xmlns="http://www.w3.org/1999/html">
<script src="public/js/orderproducts.js"></script>

<div class="container">
    <div style="margin:auto;width: 100%">

    <div class="row" style="padding: 10px">
        <div class="col-sm-1"><button class="form-control" onclick="Back()";>Terug</button></div>
    </div>

    <div class="row" style="padding: 10px">
        <div class="col-sm-3 header"> Productnaam</div>
        <div class="col-sm-3 header"> Volume</div>
        <div class="col-sm-3 header"> Hoeveelheid</div>
        <div class="col-sm-3 header"> Kosten</div>
    </div>

    <?php foreach ($products as $order){?>

        <div class="row" style="padding: 10px">
            <div class="col-sm-3 data">
                <div class="inner"><?php echo $order['productname'];?></div>
            </div>
            <div class="col-sm-3 data">
                <div class="inner"><?php echo $order['volume'];?> ML</div>
            </div>
            <div class="col-sm-3 data">
                <div class="inner"><?php echo $order['quantity'];?></div>
            </div>
            <div class="col-sm-3 data">
                <div class="inner">$ <?php echo $totalprice = $order['price'] * $order['quantity'] ;?></div>
            </div>
        </div>

    <?php } ?>
    </div>
</div>

<?php
include('base/footer.php');
?>
