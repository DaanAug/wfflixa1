<?php
include('base/header.php');
include('base/navbar.php');
?>

    <link rel="stylesheet" type="text/css" href="public/css/userspage.css" xmlns="http://www.w3.org/1999/html">

    <div class="container">
    <div style="margin:auto;width: 100%">
        <div class="row justify-content-center">
            <nav class="navbar">
                <div class="container-fluid">
                    <form action="orders" method="GET">
                        <div class="col-sm-3 ms-auto"><input id="search" class="form-control" name="search" type="text" placeholder="Zoek hier op gebruikersnaam"></div>
                        <div class="col-sm-2 ms-auto"><input id="submit" class="form-control" type="submit" value="Zoeken"></div>
                    </form>
                </div>
            </nav>
        </div>

        <div class="row" style="padding: 10px">
            <div class="col-sm-2 header"> Gebruikersnaam</div>
            <div class="col-sm-3 header"> Email</div>
            <div class="col-sm-3 header"> Aangemaakt </div>
            <div class="col-sm-4 header"> Acties </div>
        </div>

        <?php foreach ($orders as $order){?>

            <div class="row" style="padding: 10px">
                <div class="col-sm-2 data">
                    <div class="inner"><?php echo $order['username'];?></div>
                </div>
                <div class="col-sm-3 data">
                    <div class="inner"><?php echo $order['email'];?></div>
                </div>
                <div class="col-sm-3 data">
                    <div class="inner"><?php echo $order['createdAt'];?></div>
                </div>
                <div class="col-sm-2 data">
                    <div class="inner">
                        <div class="inner">
                            <input name="id" type="text" hidden value="<?php echo $order['id'];?>">
                            <a href="orderproducts?id=<?php echo $order['id'];?>"><button class="form-control" value="View Order">View Order</button></a>
                        </div>
                    </div>
                </div>
                <form method="post">
                    <div class="col-sm-2 data">
                        <div class="inner">
                            <input name="id" type="text" hidden value="<?php echo $order['id'];?>">
                            <input name="<?php echo $order['id'];?>" class="form-control" type="submit" value="Delete">
                        </div>
                    </div>
                </form>
            </div>

        <?php } ?>
    </div>
    </div>

<?php include('base/footer.php'); ?>