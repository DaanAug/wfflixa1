<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>

    <head>
        <title>Beheerders instellingen</title>
        <link rel="stylesheet" href="public/css/Admin.css"/>

    </head>
    <div class="adminTitle" style="color:whitesmoke">
        Beheerders instellingen
    </div>



    <div class="container">
        <div class="row justify-content-center">
            <br>
            <div class="col-md-3 ms-auto">
                <br>
                <input onclick="location.href='addCourse'" type="button" value="Course aanmaken" class="adminNavBar">
                <br>
                <input onclick="location.href='editCourse'" type="button" value="Course wijzigen" class="adminNavBar">
                <br>
                <input onclick="location.href='userspage'" type="button" value="Gebruikers" class="adminNavBar">
            </div>

            <div class="col-md-9 ms-auto adminProducts">
                <div class="row adminAddProduct" style="color:whitesmoke">
                    <form class="form" action="" method="post" enctype="multipart/form-data">

                        <div class="adminAddProductTitle">
                            Course toevoegen
                        </div>

                        <div class="col-md-6 adminAddProductInputs">

                            Course naam
                            <input type="text" class="form-control adminAddProductInputTitle" name="title">
                            <br>

                            Course beschrijving
                            <textarea type="text" rows="5" cols="25" name="description" class="form-control adminAddProductInputDescr" style="resize: none;"></textarea><br>
                            <div class="col-md-6">
                        </div>
                            <br>
                            <input type="submit" class="adminAddProductUpload" name="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



<?php include('views/Base/Footer.php') ?>