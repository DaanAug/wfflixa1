<?php include('views/Base/Header.php') ?>
<?php include('views/Base/Navbar.php') ?>

<div class="container">
    <h3></h3>
    <form method="post" name="editproduct" enctype="multipart/form-data">
        <div class="container">
            <div class="col-md-9 ms-auto adminProducts">
                <div class="row adminAddProduct">
                    <div class="adminAddProductTitle">
                        <p><b>Upload Video</b></p>
                    </div>

                    <div class="col-md-6 adminAddProductInputs">


                        <div style="padding: 8px">
                            <p><b>Titel</b></p>
                            <input type="text" name="title" placeholder="Titel" Required>
                        </div>
                        <div style="padding: 8px">
                            <p><b>Beschrijving</b></p>
                            <textarea name="description" rows="5" cols="25" placeholder="Beschrijving" Required></textarea>
                        </div>

                        <div style="padding: 8px">
                            <p><b>Course</b></p>
                            <select name="course" id="course">
                                <?php

                                foreach ($courseslist as $course){  ?>
                                <option value="<?= $course['courseID']?>"><?= $course['title']?></option>
                                <?php } ?>


                            </select>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <p><b>Video</b></p>
                        <b><label for="fileToUpload">Select a file:</label></b>
                        <b><input type="file" name="fileToUpload" id="fileToUpload"></b>


                        <br>

                        <input type="submit" class="adminAddProductUpload" value="Upload" name="submit">
                    </div>
                </div>
            </div>
        </div>
</div>
