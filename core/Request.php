<?php

class Request
{
    public static function uri()
    {
        if($_SERVER["SERVER_NAME"] != 'localhost') {
            $server = substr(($_SERVER['REQUEST_URI']),9);
        }
        else $server = $_SERVER['REQUEST_URI'];

        return trim(
            parse_url(
                $server, PHP_URL_PATH), "/");
    }
}